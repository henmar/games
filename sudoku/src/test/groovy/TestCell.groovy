import org.codehaus.groovy.runtime.powerassert.PowerAssertionError
import org.junit.Test
import static org.junit.Assert.*

/**
 * Created by henrik on 31-12-2015.
 */
class TestCell {

    @Test
    void testIsValidReturnsTrueForValidCharacterInput(){
        ('1'..'9').each {
            assert Cell.isValid(it as char)
        }
        assert Cell.isValid(' ' as char)
    }

    @Test
    void testIsValidReturnsFalseForInValidCharacterInput(){
        assert !Cell.isValid(null)
        ('a'..'z').each {
            assert !Cell.isValid(it as char)
        }
        ('A'..'Z').each {
            assert !Cell.isValid(it as char)
        }
        assert !Cell.isValid('0' as char) // zero is not a valid Sudoku value
        assert !Cell.isValid('æ' as char)
        assert !Cell.isValid('ø' as char)
        assert !Cell.isValid('å' as char)
        assert !Cell.isValid('Æ' as char)
        assert !Cell.isValid('Ø' as char)
        assert !Cell.isValid('Å' as char)
        assert !Cell.isValid('_' as char)
        assert !Cell.isValid('+' as char)
        assert !Cell.isValid('/' as char)
    }

    @Test
    void testConstructACellWithDefaultConstructor(){
        Cell testCell = new Cell()
        assert testCell
        assert testCell.value == (' ' as char)
        assert testCell.isEmpty()
        assert testCell.mutable
    }

    @Test
    void testConstructACellWithCharacterConstructor(){
        Cell testCell = null
        ('1'..'9').each{
            testCell = new Cell(it as char)
            assert testCell
            assert testCell.value == (it as char)
            assert !testCell.isEmpty()
            assert !testCell.mutable
        }
        testCell = new Cell (' ' as char)
        assert testCell
        assert testCell.value == (' ' as char)
        assert testCell.isEmpty()
        assert testCell.mutable
    }

    @Test
    void testConstructACellWithCharacterConstructorThrowsPAEforInvalidValue(){
        try {
            Cell testCell = new Cell('a' as char)
            fail('PowerAssertionError was expected')
        } catch (PowerAssertionError pae){
            //success
        }
    }

    @Test
    void testIsEmptyReturnsFalseForValueNotEqualToSpace(){
        Cell testCell = new Cell()
        assert testCell.isEmpty()
        ('1'..'9').each {
            testCell.setValue(it as char)
            assert !testCell.isEmpty()
        }
    }

    @Test
    void testIsEmptyReturnsTrueForValueEqualToSpace(){
        Cell testCell = new Cell()
        assert testCell.value == (' ' as char)
        assert testCell.isEmpty()
    }

    @Test
    void testSetValueThrowsPAEForInvalidValue(){
        Cell testCell = new Cell()
        assert testCell
        try{
            testCell.setValue('a' as char)
            fail('PowerAssertionError was expected')
        } catch (PowerAssertionError pae) {
            //success
        }
    }
}
