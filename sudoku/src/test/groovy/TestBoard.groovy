import org.junit.Test

class TestBoard{

    @Test
    void testConstructBoardFromListOfStrings() {
        List<String> s = []
        s[0] = '  9   1 6'
        s[1] = '   71 8 3'
        s[2] = '3  96   2'
        s[3] = '  6 5 3 8'
        s[4] = '  5   4  '
        s[5] = '8 7 4 5  '
        s[6] = '5   84  1'
        s[7] = '9 8 31   '
        s[8] = '6 3   2  '

        Board testBoard = new Board(s)
        assert testBoard.getCell(0,0).value == ' ' as char
        assert testBoard.getCell(0, 0).mutable
        assert testBoard.getCell(3,2).value == '9' as char
        assert !testBoard.getCell(3, 2).mutable
    }

}