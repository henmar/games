import org.junit.Test

/**
 * Created by henrik on 02-01-2016.
 */
class TestRow {

    @Test
    void testIsSolvedReturnsTrueForRowWith9UniqueNonEmptyValues(){
        def cells = []
        ('1'..'9').each{
            cells.add(new Cell(it as char))
        }
        Row testRow = new Row(cells)
        assert testRow.isSolved()
    }

    @Test
    void testIsSolvedReturnsFalseForRowWith8UniqueNonEmptyValuesAndOneBlank(){
        def cells = []
        ('1'..'8').each{
            cells.add(new Cell(it as char))
        }
        cells.add(new Cell(' ' as char))
        Row testRow = new Row(cells)
        assert !testRow.isSolved()
    }

    @Test
    void testConstructorThrowsErrorforShortListOfCells(){

    }
}
