class Row {
  List<Cell> cells = []
  
  Row (List<Cell> cells){
    assert cells
    assert cells.size() == 9
    this.cells = cells
  }
  
  Boolean isValid(){
    def nonEmpty = cells.findAll{!it.isEmpty()}
    def uniqueCells = nonEmpty.toUnique()
    return nonEmpty.size() == uniqueCells.size{a, b -> a.value <=> b.value}
  }
  
  Boolean isSolved(){
    Boolean solved = false
    def nonEmpty = cells.findAll{!it.isEmpty()}
    if (nonEmpty.size() == 9){
      def uniqueCells = nonEmpty.toUnique{a, b -> a.value <=> b.value}
      if (uniqueCells.size() == 9){
        solved = true
      }
    }
    return solved
  }
  
  void setCell(Integer index, Cell cell){
    
    throw new UnsupportedOperationException('Not Implemented yet...')
  }
  
}