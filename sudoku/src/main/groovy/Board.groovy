class Board {
 
  //Region names
  static final String UPPER_LEFT    = 'upperLeft'
  static final String UPPER_CENTER  = 'upperCenter'
  static final String UPPER_RIGHT   = 'upperRight'
  static final String MIDDLE_LEFT   = 'middleLeft'
  static final String MIDDLE_CENTER = 'middleCenter'
  static final String MIDDLE_RIGHT  = 'middleRight'
  static final String LOWER_LEFT    = 'lowerLeft'
  static final String LOWER_CENTER  = 'lowerCenter'
  static final String LOWER_RIGHT   = 'lowerRight'
  //TODO could have been a list of strings
  
  //TODO Regions as a map of Region, their names as key
  //     Rows as a List of Row indexed from top to bottom
  //     Columns as List of Column indexed from left to right
  //     all of the three above structures should be "singletons"
  //     all of them should reference the same 91 Cells - in various dimensions and directions
  //     When a cell in a Row is changed, the corresponding cell in the intersecting column is thus changed as well (after all, it is the same cell) - same goes for regions
  //     A row "knows" if it is valid, a row knows if it is complete - same goes for columns and regions
  
  private List<Cell> cells = []
  private List<Row>  rows  = []
  private List<Column> columns = []
  private Map<String, Region> regions = [:]
  
  Board(){
    (0..80).each{ index ->
      cells[index] = new Cell()
    }
    assert cells.size() == 81
  }
  
  Board(List<String> rows){
    this()
    assert rows.size == 9
    rows.eachWithIndex{ row, rowIndex ->
      assert row.size() == 9
      Character value = null
      (0..8).each{ columnIndex ->
        value =  row[columnIndex] as char
        cells[rowIndex * 9 + columnIndex].setValue(value)
        cells[rowIndex * 9 + columnIndex].mutable = value == (' ' as char)
      }
    }
  }
  
  Cell getCell(Integer index){
    return cells[index]
  }
  
  Cell getCell(Integer columnIndex, Integer rowIndex){
    return cells[rowIndex * 9 + columnIndex]
  }
  
  Row getRow(Integer index){
    /*assert (index >= 0) && (index < 9) 
    List<Cell> row = []
    def ids = [${8*index + 0}..${8*index + 8}]
    ids.each{ id ->
      row << cells[id]
    }*/
    return rows[index]
  }
  
  Column getColumn(Integer index){
    return columns[index]
  }
  
  Region getRegion(String name){
    return regions[name]
  }
  
}

