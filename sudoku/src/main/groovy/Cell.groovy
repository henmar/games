class Cell{
    Character value = null
    Boolean mutable = true


    /**
     * Creates an instance of Cell, with value set to blank
     * Cells instantiated with this constructor are set to be "mutable" - reflecting
     * cells on the Sudoku board whose values are to be guessed - as opposed to the
     * cells with pre-filled values which serve the purpose of being clues to guess
     * the values of the remaining cells.
     * @param c Character - valid values are ´0´ - ´9´ and space
     */
    public Cell(){
      this.value = ' ' as char
      this.mutable = true
    }

    /**
     * Creates an instance of Cell, with value set according to the parameter c
     * Cells instantiated using this constructor, with value parameter values different
     * from space are set to be "immutable" - reflecting cells on the Sudoku board
     * which are pre-filled and thus serve as clues to guess the remaining cells
     * @param c Character - valid values are ´0´ - ´9´ and space
     */
    public Cell(Character value){
      assert value
      assert isValid(value)
      this.value = value
      this.mutable = value == (' ' as char)
    }

    /**
     * Changes the value of this Cell,  according to the parameter c
     * @param c Character - valid values are ´0´ - ´9´ and space
     */
    void setValue(Character value){
      assert mutable
      assert value
      assert isValid(value)
      this.value = value
    }

    /**
     * Convenience method for clients to check whether a propopsed value is Valid or not
     * @param c
     * @return
     */
    static boolean isValid(Character value){
      def valid = false
      if (value){
        if (value == (' ' as char)){
          valid = true
        } else {
            if (('' + value).isNumber()){
                if (value != ('0' as char)){

                    valid = true
                }
            }
        }
      }
      return valid
    }
    
    boolean isEmpty(){
      return this.value == (' ' as char)
    }
  }