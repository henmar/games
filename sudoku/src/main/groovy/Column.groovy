class Column {
  List<Cell> cells = []
  
  Column (List<Cell> cells){
    assert cells
    assert cells.size() == 9
    this.cells = cells
  }
  
  Boolean isValid(){
    def nonEmpty = cells.findAll{!it.isEmpty()}
    def uniqueCells = nonEmpty.unique(mutate = false)
    return nonEmpty.size() == uniqueCells.size()
  }
  
  Boolean isSolved(){
    Boolean solved = false
    def nonEmpty = cells.findAll{!it.isEmpty()}
    if (nonEmpty.size() == 9){
      def uniqueCells = nonEmpty.unique(mutate = false)
      if (uniqueCells.size == 9){
        solved = true
      }
    }
    return solved
  }
  
  void setCell(Integer index, Cell cell){
    
    throw new UnsupportedOperationException('Not Implemented yet...')
  }
  
}